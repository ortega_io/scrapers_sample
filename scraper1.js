/*

/usr/local/bin/casperjs --ignore-ssl-errors=true --ssl-protocol=any --cookies-file=/home/otto/Desktop/Scraper/cookie.txt /home/otto/Desktop/Scraper/scraper.js

*/


/* Define routing URLs and global variables ================================= */

var user 			= "shivam@writermailbox.com";
var password 		= "qpalzm0101";

var tempIpsIds		= [];
var loginPage 		= "https://secure.domaintools.com/log-in/";
var loginTitle		= "Log in to DomainTools";
var myAccountUrl	= "https://account.domaintools.com/my-account/";
var ipMonitorUrl 	= "https://research.domaintools.com/monitor/ip-monitor/";
var ipMonitorInit 	= "https://research.domaintools.com/?ajax=mIPMonitor&call=ajaxInitialize";
var ipMonitorRefer 	= "https://research.domaintools.com/monitor/ip-monitor/changes/?q=";
var newIpsUrl 		= "https://research.domaintools.com/?ajax=mIPMonitor&call=ajaxLoadIP";
var addIpsUrl 		= "https://research.domaintools.com/?ajax=mIPMonitor&call=ajaxAddIPs";
var delIpsUrl 		= "https://research.domaintools.com/?ajax=mIPMonitor&call=ajaxDeleteIPs";
var csrfToken 		= "";



/* Date related tasks ======================================================= */

var today 	= new Date();
var dd 		= today.getDate();
var mm 		= today.getMonth()+1; //January is 0!
var yyyy 	= today.getFullYear();

currentDate = yyyy+'-'+mm+'-'+dd;

var system  		= require('system');
var args    		= system.args;
var monitoredIps	= args[4].split("|");


/* Set up CasperJS instance and Utils object ================================ */

var utils 	= require('utils');
var casper 	=

require('casper').create
({
	verbose	: true,
	logLevel: 'error',
	pageSettings:
	{
		loadImages	: false,
		loadPlugins	: false,
		userAgent	: 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:36.0) Gecko/20100101 Firefox/36.0'
	}
});



/* Load login page ========================================================== */

casper.start
(
	loginPage,
	function()
	{

		var pageTitle = this.getTitle();

		if(pageTitle!=loginTitle)
		{
			this.echo("* ERROR: Could not load login page");
			this.exit();
		}

    }
);



/* Login to Domaintools.com ================================================= */

casper.then
(
	function()
	{

		this.click('input[id="username"]');
		this.fill
		(
			'form[id="login"]',
			{
				'username'	: user,
				'password'	: password
			},
			false
		);

		this.click('#login-button');

	}
);



casper.waitFor
(
	function()
	{
		var pageUrl = this.getCurrentUrl();

		if(pageUrl==myAccountUrl)
		{
			return true;
		}
		else
		{
			return false;
		}
	},
	function ()
	{
		var elapse = Math.ceil(Math.random() * 4000);

	    this.wait
	    (
	    	elapse,
	    	function()
	    	{
	        	// finished waiting //
	    	}
	    );
	}
	,
	function ()
	{
		this.echo("* ERROR: Unable to load account");
		casper.exit();
	}
);




/* IP MONITOR SERVICE [START] =============================================== */
/* ========================================================================== */


/* Initialize IP Monitoring Module ========================================= */


casper.thenOpen
(
	ipMonitorUrl
);


casper.waitFor
(
	function()
	{

		if(this.exists('th.ip-column'))
		{
			return true;
		}
		else
		{
			return false;
		}

	},
	function ()
	{
		var elapse = Math.ceil(Math.random() * 2500);

	    this.wait
	    (
	    	elapse,
	    	function()
	    	{
	        	// finished waiting //
	    	}
	    );
	}
	,
	function ()
	{
		this.echo('* ERROR: Unable to load IP Monitoring Service');
		casper.exit();
	}
);



casper.thenOpen
(
	ipMonitorInit,
	{
	    method	: 'post',
	    data	:
	    {

	    }
	},
	function()
	{

		var cookie 	= this.evaluate(function(){ return document.cookie;});
		var result 	= /csrftoken=(.*?);/.exec(cookie);

		if(result.length==2)
		{
		    csrfToken = result[1];
		}
		else
		{
			this.echo("* ERROR: unable to match XCRSF token");
			casper.exit();
		}

	}
);



/* Load list of IPs to monitor ============================================== */

casper.thenOpen
(
	addIpsUrl,
	{
	    method	: 'post',
	    data	:
	    {
	        'ips[]'	: monitoredIps
	    }
	},
	function()
	{

		var jsonReply 	= this.getElementInfo('body').text;

		try
		{
			var data	= JSON.parse(jsonReply);
		}
		catch(error)
		{
			this.echo("* ERROR: Unable to parse JSON on add IPs");
			return;
		}

		if( (typeof data == 'undefined') || (typeof data.response == 'undefined') || (typeof data.response.object == 'undefined') )
		{
			this.echo("* ERROR: Got empty reply on add IPs");
			this.echo(jsonReply);
			return;
		}

		var result 		= data.type;


		if(result!='success')
		{
			this.echo("* ERROR: Unable to add IPs");
		}

		var ipIds 	= data.response.object.entries;

		for(var k = 0; k<ipIds.length; k++)
		{
			tempIpsIds.push(ipIds[k].id);
		}


		/* Random Delay */

		var elapse = Math.ceil(Math.random() * 2500);

	    this.wait
	    (
	    	elapse,
	    	function()
	    	{
	        	// finished waiting //
	    	}
	    );

	}
);



/* Get all new domains available ============================================ */

casper.then
(
	function()
	{
		var index = -1;

 		this.each
 		(
 			monitoredIps,
 			function()
 			{

 				index++;

 				var currentIp 	= monitoredIps[index];
 				var referer 	= ipMonitorRefer+currentIp;

 				casper.page.customHeaders = { 'X-CSRFToken' : csrfToken, 'Referer' : referer };

				this.thenOpen
				(
					newIpsUrl,
					{
					    method	: 'post',
					    data	:
					    {
					        'date'	: currentDate,
					        'ip'	: currentIp,
					        'page'	: 0,

					    }
					},
					function()
					{

						var jsonReply 	= this.getElementInfo('body').text;

						try
						{
							var data		= JSON.parse(jsonReply);
						}
						catch(error)
						{
							this.echo("* ERROR: Unable to parse JSON");
							return;
						}

						if( (typeof data == 'undefined') || (typeof data.response == 'undefined') || (typeof data.response.object == 'undefined') || (typeof data.response.object.changes == 'undefined') )
						{
							this.echo("* ERROR: Got empty reply on IP loop");
							this.echo("["+jsonReply+"]");
							return;
						}

						var changes 	= data.response.object.changes;
						var pageCount	= data.response.object.pages;

						if(changes.length>0)
						{
							this.echo("* IP: "+currentIp);
						}

						for(var k = 0; k<changes.length; k++)
						{
							var change = changes[k];
							this.echo(change.domain);
						}

						var pages 	   = [];

						if(pageCount>1)
						{

							for(var j =1; j<pageCount; j++)
							{
								pages.push(j);
							}


					 		var pageIndex = -1;

					 		this.each
					 		(
					 			pages,
					 			function()
					 			{
					 				pageIndex++;

									this.thenOpen
									(
										newIpsUrl,
										{
										    method	: 'post',
										    data	:
										    {
										        'date'	: currentDate,
										        'page'	: pages[pageIndex],
										        'ip'	: currentIp
										    }
										},
										function()
										{


											var jsonReply 	= this.getElementInfo('body').text;
											var data		= JSON.parse(jsonReply);
											var changes 	= data.response.object.changes;

											for(var k = 0; k<changes.length; k++)
											{
												var change  = changes[k];

												this.echo( change.domain );
											}

										}
									);

									var elapse = Math.ceil(Math.random() * 2500);

								    this.wait
								    (
								    	elapse,
								    	function()
								    	{
								        	// finished waiting //
								    	}
								    );

					 			}

					 		);

						}


						var elapse = Math.ceil(Math.random() * 2500);

					    this.wait
					    (
					    	elapse,
					    	function()
					    	{
					        	// finished waiting //
					    	}
					    );

					}
				);
 			}

 		);

	}
);




/* Remove List of IPs ======================================================= */

casper.thenOpen
(
	delIpsUrl,
	{
	    method	: 'post',
	    data	:
	    {
	        'ids[]'	: tempIpsIds
	    }
	},
	function()
	{

		var jsonReply 	= this.getElementInfo('body').text;

		try
		{
			var data	= JSON.parse(jsonReply);
		}
		catch(error)
		{
			this.echo("* ERROR: Unable to parse JSON on delete IPs");
			return;
		}

		if( (typeof data == 'undefined') || (typeof data.response == 'undefined') || (typeof data.response.object == 'undefined') )
		{
			this.echo("* ERROR: Got empty reply on delete IPs");
			return;
		}

		var result 		= data.type;


		if(result!='success')
		{
			this.echo("* ERROR: Unable to remove IPs");
		}


		/* Random Delay */

		var elapse = Math.ceil(Math.random() * 2500);

	    this.wait
	    (
	    	elapse,
	    	function()
	    	{
	        	// finished waiting //
	    	}
	    );

	}
);


/* IP MONITOR SERVICE [END] ================================================= */
/* ========================================================================== */


casper.run();
